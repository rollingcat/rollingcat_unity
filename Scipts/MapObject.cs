﻿using UnityEngine;
using System.Collections;

public class MapObject : MonoBehaviour {
	private int mytype;
	Transform myTransform;

	public Gobject m_mapObjectAttr;

	GameManager		m_gameManager;

	public void SetMapObjectAttribute(Gobject attr, GameManager gm) {
		m_mapObjectAttr = attr;
		m_gameManager = gm; 
	}

	// Use this for initialization
	void Start () {
		myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {
		myTransform.position.Set (myTransform.position.x, myTransform.position.y, m_gameManager.g_Zoom/200f); 
	}
}
