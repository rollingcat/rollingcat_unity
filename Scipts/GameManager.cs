using UnityEngine;
using System.Collections;
using System;
//파일입출력을 위해
using System.IO;
//Binary형식을 위해
using System.Runtime.Serialization.Formatters.Binary;
//List사용을 위해
using System.Collections.Generic;

//chi test


public class GameManager : MonoBehaviour {
	static public int m_gameStart = 0;
	public int m_mapIndex = 0;


	// map sprite
	public Transform m_farbg1;
	public Transform m_closebg1;
	public Transform m_farbg2;
	public Transform m_closebg2;
	// Map object
	public GameObject m_mapObjectPrefab;



	Vector3 m_postionStart;
	Vector3 m_postionEnd;
	Vector3 m_postionCloseStart;
	Vector3 m_postionCloseEnd;


	public float     PTM_ratio; // PIXEL TO METER ratio
	public float     MTP_ratio; // METER TO PIXEL ratio
	public float     OFFSET_PIXEL_X; // Anchor (Bottom and Left)
	public float     OFFSET_PIXEL_Y; // Anchor (Bottom and Left)
	
	public float     bg_pos_x;  // 인게임 시작위치
	public float     bg_pos_y;  // 



	public List <GameObject> m_mapObjects;
	public List <GameObject> m_objectsDisplay;


	public float m_changeDistance = 0;
	public float m_lastObjectPosition = 0;

	Dictionary<string, Sprite> m_imageDics = new Dictionary<string, Sprite>();

		
	public int g_BottomZoom=0;
	public int g_Zoom=0;



	void SetUnit()
	{

	}

	// Use this for initialization
	void Start () {
		// initialize variables for pixel viewport. prepare also meter stuff 

		g_BottomZoom=0;
		g_Zoom=0;

		m_mapIndex = 0;
		Debug.Log("Screen : " + Screen.width+"," + Screen.height);

		//MTP_ratio = Screen.height / (camera.orthographicSize *2);
		MTP_ratio = 540 / (camera.orthographicSize *2);
		PTM_ratio = 1.0f/MTP_ratio;
		OFFSET_PIXEL_X = -Screen.width / 2.0f;
		OFFSET_PIXEL_Y = -Screen.height / 2.0f; 
		bg_pos_x = 0;
		bg_pos_y = 0;

		//camera.transform.localScale = new Vector3(960f/Screen.height, 540f/Screen.height, 0f);


		m_changeDistance = 0;
		m_lastObjectPosition = 0;
		// loading map data

		string mapName  = "easy_"+ m_mapIndex;

		TextAsset asset = Resources.Load(mapName) as TextAsset;

		if (asset != null) {
			Debug.Log("Debug : " + asset.bytes.Length);
		} 
		else {
			Debug.Log ("NULL");

			return;
		}
		/*MemoryStream stream  = new MemoryStream(asset.bytes);
		BinaryReader br = new BinaryReader(stream);
		int slen = (int)stream.Length;
		Debug.Log("Debug len: " + slen);
		br,Read
		stream.Dispose();
		br.Close();*/

		m_mapObjects = new List<GameObject>();

		int length = asset.bytes.Length;
		short objectCount = BitConverter.ToInt16(asset.bytes, 0);
		Debug.Log ("count : " + objectCount);

		int readSize  = 2;
		for (int i = 0 ; i < objectCount; i++) {
			int objectType = BitConverter.ToInt32(asset.bytes, readSize); readSize+=4;
			int objectIndex = BitConverter.ToInt32(asset.bytes, readSize);  readSize+=4;

			int objectX = BitConverter.ToInt32(asset.bytes, readSize);  readSize+=4;
			int objectY = 540-BitConverter.ToInt32(asset.bytes, readSize);  readSize+=4;

			int objectPerAngle = BitConverter.ToInt32(asset.bytes, readSize);  readSize+=4;
			int objectScale = BitConverter.ToInt32(asset.bytes, readSize);  readSize+=4;
			int objectZoom = BitConverter.ToInt32(asset.bytes, readSize);  readSize+=4;

			//Debug.Log ("#"+i+":"+objectType+","+objectIndex+","+objectX+","+objectY+","+objectPerAngle+","+objectScale+","+objectZoom);

			Gobject obj  = new Gobject(objectType,objectIndex,objectX, objectY,objectPerAngle,objectScale, objectZoom);
			GameObject mapObj = (GameObject)Instantiate (m_mapObjectPrefab);
			mapObj.GetComponent<MapObject>().SetMapObjectAttribute(obj, this);

			mapObj.transform.position = new Vector3((float)objectX*PTM_ratio,(float)objectY*PTM_ratio, 0);

			int colliderType = (int)ColliderName.Circle; //
			string key;
			Sprite spriteValue;

			if (objectType == (int)GobjectName.Candy) {
				key = "candy/"+(objectIndex+1);
			}
			else if (objectType == (int)GobjectName.Jelly) {
				key = "jelly/"+(objectIndex+1);
			}
			else if (objectType == (int)GobjectName.Item) {
				key = "item/item"+(objectIndex+1);
			}
			else if (objectType == (int)GobjectName.Cloud) {
				key = "cloud/cloud"+(objectIndex+1);
				colliderType = (int)ColliderName.Box;
			}
			else if (objectType == (int)GobjectName.Land) {
				key = "land/land"+(objectIndex+1);
				colliderType = (int)ColliderName.Box;
			}
			else {
				key = "fire/fire"+(objectIndex+1);
				colliderType = (int)ColliderName.Box;
			}

			if (m_imageDics.ContainsKey(key)==false) {
				spriteValue = Resources.Load(key, typeof(Sprite)) as Sprite;
				m_imageDics.Add (key, spriteValue);
			}
			else {
				spriteValue = (Sprite)m_imageDics[key];
			}

			if (spriteValue == null) {
				Debug.Log ("Sprite key : " + key);
				continue;
			}


			float width  = spriteValue.rect.width;
			float height = spriteValue.rect.height;

			if (colliderType == (int)ColliderName.Circle) {
				mapObj.GetComponent<CircleCollider2D>().enabled = true;
			}
			else {
				mapObj.GetComponent<BoxCollider2D>().enabled = true;
			}
			mapObj.GetComponent<SpriteRenderer>().sprite = spriteValue;
			mapObj.GetComponent<SpriteRenderer>().enabled = true;

			m_mapObjects.Add(mapObj);
		}

		GameObject candy = (GameObject)m_mapObjects[0];
		g_BottomZoom = candy.GetComponent<MapObject>().m_mapObjectAttr.Zoom;

		for (int i = 0; i <= 1; i++) {

			Transform bg;
			Transform bgClose;
			if (i == 0) {
				bg = m_farbg1;
				bgClose = m_closebg1;

			}
			else {
				m_farbg2.Translate(960.0f*PTM_ratio, 0f, 0f);
				m_closebg2.Translate(960.0f*PTM_ratio, 0f, 0f);

				m_postionStart = m_farbg2.position;
				m_postionEnd = new Vector3(-m_postionStart.x, m_postionStart.y, m_postionStart.z);

				bg = m_farbg2;
				bgClose = m_closebg2;

				Debug.Log("m_postionStart : "+m_postionStart.x);
				Debug.Log("m_postionEnd : "+m_postionEnd.x);
			}
			string bgname  = "bg"+(m_mapIndex+1)+"/"+"farbg"+(i+1);
			Debug.Log("bg image name : "+bgname);
			SpriteRenderer bg1 = bg.GetComponent<SpriteRenderer>();
			bg1.sprite = Resources.Load(bgname, typeof(Sprite)) as Sprite;

			bgname  = "bg"+(m_mapIndex+1)+"/"+"closebg"+(i+1);
			Debug.Log("bg image name : "+bgname);
			SpriteRenderer bgClose1 = bgClose.GetComponent<SpriteRenderer>();
			bgClose1.sprite = Resources.Load(bgname, typeof(Sprite)) as Sprite;

			Debug.Log("bgClose1.sprite.rect.height : "+bgClose1.sprite.rect.height + ", width :" + bgClose1.sprite.rect.width);
			//bgClose1.sprite.rect.height/2
			bgClose.Translate(0f, -(540f/2 - bgClose1.sprite.rect.height/2.0f)*PTM_ratio , 0f);//+ (bgClose1.sprite.rect.height/2.0f)*PTM_ratio

			if (i == 1) {
				m_postionCloseStart = bgClose.position;
				m_postionCloseEnd = new Vector3(-m_postionCloseStart.x, m_postionCloseStart.y, m_postionCloseStart.z);
			}
		}

		m_gameStart = 1;




	}


	void updateBackground(float dt) {

		Vector3 pos1 = m_farbg1.position;
		Vector3 pos2 = m_farbg2.position;
		
		if (pos1.x <= m_postionEnd.x) {
			m_farbg1.position = m_postionStart;
		}
		
		if (pos2.x <= m_postionEnd.x) {
			m_farbg2.position = m_postionStart;
		}
		
		m_farbg1.Translate(-dt*PTM_ratio*20, 0, 0);
		m_farbg2.Translate(-dt*PTM_ratio*20, 0, 0);
		
		
		pos1 = m_closebg1.position;
		pos2 = m_closebg2.position;
		
		if (pos1.x <= m_postionCloseEnd.x) {
			m_closebg1.position = m_postionCloseStart;
		}
		
		if (pos2.x <= m_postionCloseEnd.x) {
			m_closebg2.position = m_postionCloseStart;
		}
		
		m_closebg1.Translate(-dt*PTM_ratio*50, 0, 0);
		m_closebg2.Translate(-dt*PTM_ratio*50, 0, 0);
	}

	void updateObject (float dt) {
		//0813
		//오브젝트의 줌 값에 따라 설정 값만큼 줌 조절
		if ( g_BottomZoom == 1 ){
			if ( g_Zoom < 340 ){
				g_Zoom += (int)(20*dt);
			}
			
			else if ( g_Zoom > 340 ){
				g_Zoom -= (int)(20*dt);
			}
		}
		else if ( g_BottomZoom == 2 ){
			if ( g_Zoom < 440 ){
				g_Zoom += (int)(20*dt);
			}
			
			else if ( g_Zoom > 440 ){
				g_Zoom -= (int)(20*dt);
			}
		}
		
		else if ( g_BottomZoom == 3 ){
			if ( g_Zoom < 460 ){
				g_Zoom += (int)(20*dt);
			}
			
			else if ( g_Zoom > 460 ){
				g_Zoom -= (int)(20*dt);
			}
		}
		
		else {
			if ( g_Zoom > 150 ){
				g_Zoom -= (int)(20*dt);
			}
		}
	}




	// Update is called once per frame
	void Update () {
		float dt = Time.deltaTime;

		m_changeDistance += dt;

		updateBackground(dt);
		updateObject(dt);

	}



	public void SaveGameData()
	{
		/*PlayerPrefs.SetInt("BGM", BGM ? 1 : 0);
		PlayerPrefs.SetInt("EFFECT", EFFECT ? 1 : 0);
		PlayerPrefs.SetInt("VIBRATION", VIBRATION ? 1 : 0);
		PlayerPrefs.SetInt("FeverCount", FeverCount);
		PlayerPrefs.SetString("Language", Language);
		PlayerPrefs.SetString("UserID", UserID);
		
		//
		PlayerPrefs.SetInt("VIPEnd", VIPEnd ? 1 : 0);*/
	}
	
	
	public void initializationData()
	{	
		/*PlayerPrefs.SetInt("BGM", 1);
		PlayerPrefs.SetInt("EFFECT", 1);
		PlayerPrefs.SetInt("VIBRATION", 1);
		PlayerPrefs.SetInt("FeverCount", 0);
		PlayerPrefs.SetString("Language", "Chinese");
		PlayerPrefs.SetString("UserID", "TEST");	
		
		//
		PlayerPrefs.SetInt("VIPEnd", 0);*/
		
	}	

	//  싱글턴 패턴을 적용시킨다

	/*private static GOBJECT p_Instance = null;  
	public static GOBJECT instance {
		
		get {
			
			if (p_Instance == null) {
				
				p_Instance =  FindObjectOfType(typeof (GOBJECT)) as GOBJECT;
				
			}
			if (p_Instance == null) {
				
				GameObject obj = new GameObject("AManager");
				p_Instance = obj.AddComponent(typeof (GOBJECT)) as GOBJECT;
				
			}
			
			return p_Instance;
			
		}
	}*/

}








