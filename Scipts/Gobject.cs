﻿using UnityEngine;


public enum GobjectName {
	Candy=1,
	Jelly,
	Item,
	Cloud,
	Land,
	Fire
}

public enum ColliderName {
	Circle,
	Box
}

public class Gobject
{
	
	public Gobject(int type, int index, int x, int y, int angle, int scale, int zoom)
	{
		this.Type = type;
		this.Index = index;
		this.x = x;
		this.y = y;
		this.scale = angle;
		this.scale = scale;
		this.Zoom = zoom;
		
		this.orgType = this.Type;
		this.oldx = x;
		this.oldy = y;
		this.movePt = Random.Range(0, 16);
		this.bReload = 0;
		this.bFireSet = 0;
	}
	
	
	public float x;
	public float y;
	
	public float oldx;
	public float oldy;
	
	public float angle;
	public int   scale;
	public int   Zoom;
	
	public float mergyX;
	public float mergyY;
	
	public int   perAngle;
	
	public int   frame;
	public int   State;
	
	public int   Type;
	public int   Index;
	
	public int	  orgType;
	public int   orgIndex;
	public int   ColIdx;
	public int   movePt;
	
	public int   bMagneticMove;
	
	public int  bReload;
	
	public int  bFireSet;
	
	public int   tmpFileName;
	
}

